import loadView from './views/loader'

function handleDOMContentLoaded () {
  const viewName = document.getElementsByTagName("body")[0].dataset.jsViewName
  const ViewClass = loadView(viewName)
  const view = new ViewClass()
  view.mount()
}

window.addEventListener("DOMContentLoaded", handleDOMContentLoaded, false)
