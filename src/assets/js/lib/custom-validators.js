let customValidators = {
  requiredCheckbox: (val) => {
    return val === true
  },

  mustEqual: (a, b) => {
    return a === b
  },

  inArrayValidator: (val, vals) => {
    return (vals.includes(val))
  },

  cardValidator: (val, cardTypeField) => {
		let cctype = (typeof cardTypeField === 'function' ? cardTypeField() : cardTypeField)
		if (!cctype || !val) return false
		cctype = cctype.toLowerCase()

		if (val.length < 15) {
			return (false)
		}
		let match = cctype.match(/[a-zA-Z]{2}/)
		if (!match) {
			return (false)
		}

		let number = val
		match = number.match(/[^0-9]/)
		if (match) {
			return (false)
		}

		let fnMod10 = function (number) {
			let doubled = []
			for (let i = number.length - 2; i >= 0; i = i - 2) {
				doubled.push(2 * number[i])
			}
			let total = 0
			for (let i = ((number.length % 2) == 0 ? 1 : 0) ; i < number.length; i = i + 2) {
				total += parseInt(number[i])
			}
			for (let i = 0; i < doubled.length; i++) {
				let num = doubled[i]
				let digit
				while (num != 0) {
					digit = num % 10
					num = parseInt(num / 10)
					total += digit
				}
			}

			if (total % 10 == 0) {
				return (true)
			} else {
				return (false)
			}
		}

		switch (cctype) {
			case 'vc':
			case 'mc':
			case 'ae':
				//Mod 10 check
				if (!fnMod10(number)) {
					return false
				}
				break
		}
		switch (cctype) {
			case 'vc':
				if (number[0] != '4' || (number.length != 13 && number.length != 16)) {
					return false
				}
				break
			case 'mc':
				if (number[0] != '5' || (number.length != 16)) {
					return false
				}
				break

			case 'ae':
				if (number[0] != '3' || (number.length != 15)) {
					return false
				}
				break

			default:
				return false
			}

		return (true)
	},

  passwordStrength: (val) => {
    return (/^(?=.*\d)(?=(.*\W){1})(?=.*[a-zA-Z])(?!.*\s).{8,}$/.test(val))
  }
}

export default customValidators
