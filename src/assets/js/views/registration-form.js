import ko from 'knockout'
import koValidation from 'knockout.validation'
import CustomValidators from '../lib/custom-validators'
import InputMaps from '../lib/input-maps'

const REGISTRATION_ENDPOINT = 'http://wrld.dummy.signup'

koValidation.rules.pattern.message = 'Invalid'

koValidation.init({
  registerExtenders: true,
  messagesOnModified: true,
  insertMessages: true,
  parseInputAttributes: true,
  messageTemplate: null
}, true)

class RegistrationViewModel {
  constructor (registrationEndpoint) {
    this.registrationEndpoint = registrationEndpoint
    this._setupObservables()
    this._setupErrorGroups()
  }

  nextStep () {
    let currentStepIdx = this.currentStep()
    let currentStepObservable = this.steps[currentStepIdx]
    let maxSteps = Object.keys(this.steps).length
    if (currentStepObservable.isValid()) {
      if (currentStepIdx === maxSteps) {
        this.submit()
      }
      else if (currentStepIdx < maxSteps) {
        this.currentStep(currentStepIdx + 1)
      }
    }
    else {
      currentStepObservable.errors.showAllMessages(true)
    }
  }

  previousStep () {
    if (this.currentStep() > 1) this.currentStep(this.currentStep() - 1)
  }

  submit () {
    if (this.errors().length === 0) {
      // Just for testing we can bypass the ajax request and
      // force the 'request' to be successful, so we can
      // view the outcome of a successful post
      // This can be done by adding the parameter force=true
      // to the url
      if (!this._forceSuccess()) {
        this._processForm()
      }
    }
  }

  valid () {
    return (this.errors().length === 0)
  }

  // Private
  
  _setupObservables () {
    this.submittingForm = ko.observable(false)
    this.submitError = ko.observable(false)
    this.completed = ko.observable(false)

    this.currentStep = ko.observable(1)

    // Personal details
    this.firstName = ko.observable().extend({ minLength: 2, maxLength: 20 })
    this.lastName = ko.observable().extend({ minLength: 2, maxLength: 20 })
    this.email = ko.observable().extend({ email: true })
    this.password = ko.observable().extend({
      validation: {
        validator: CustomValidators.passwordStrength,
        message: 'Password must be at least 8 characters long, with a letter, number and symbol',
        params: this.password
      }
    })
    this.confirmPassword = ko.observable().extend({
      validation: {
        validator: CustomValidators.mustEqual,
        message: 'Passwords do not match',
        params: this.password
      }
    })

    // Billing information
    this.addressOne = ko.observable()
    this.addressTwo = ko.observable()
    this.city = ko.observable()
    this.postCode = ko.observable()
    this.county = ko.observable().extend({ minLength: 2, maxLength: 30 })
    this.countries =ko.observable(InputMaps.countryMap)
    this.selectedCountry = ko.observable().extend({
      validation: {
        validator: CustomValidators.inArrayValidator,
        message: 'Invalid country selected',
        params: InputMaps.countryMap.map((c) => { return c.val })
      }
    })

    // Card details
    this.cvc = ko.observable().extend({ minLength: 3, maxLength: 3 })
    this.cardTypes = ko.observableArray(InputMaps.cardTypeMap)
    this.selectedCardType = ko.observable()
    this.expiryDateMonths = ko.observable(InputMaps.monthMap)
    this.terms = ko.observable(false).extend({
      validation: {
        validator: CustomValidators.requiredCheckbox,
        message: 'Must be confirmed',
        params: true
      }
    })
    this.selectedExpiryDateMonth = ko.observable().extend({
      validation: {
        validator: CustomValidators.inArrayValidator,
        message: 'Invalid month',
        params: InputMaps.monthMap.map((c) => { return c.val })
      }
    })
    this.expiryDateYears = ko.observable(InputMaps.yearMap)
    this.selectedExpiryDateYear = ko.observable().extend({
      validation: {
        validator: CustomValidators.inArrayValidator,
        message: 'Invalid year',
        params: InputMaps.yearMap.map((c) => { return c.val })
      }
    })
    this.cardNumber = ko.observable().extend({
      validation: {
        validator: CustomValidators.cardValidator,
        message: 'Invalid card number',
        params: this.selectedCardType
      }
    })

    this.errors = ko.validation.group(this)
  }

  _setupErrorGroups () {
    this.steps = {
      1: ko.validatedObservable({
           firstName: this.firstName,
           lastName: this.lastName,
           email: this.email,
           password: this.password,
           confirmPassword: this.confirmPassword
         }),
      2: ko.validatedObservable({
           addressOne: this.addressOne,
           addressTwo: this.addressTwo,
           city: this.city,
           county: this.county,
           country: this.selectedCountry,
           postCode: this.postCode
         }),
      3: ko.validatedObservable({
           expiryDateMonth: this.selectedExpiryDateMonth,
           expiryDateYear: this.selectedExpiryDateYear,
           cvc: this.cvc,
           cardType: this.selectedCardType,
           cardNumber: this.cardNumber,
           terms: this.terms
         })
    }
  }

  _forceSuccess () {
    let urlParams = new URLSearchParams(window.location.search)
    if (urlParams.has('force')) {
      this.submitError(false)
      this.completed(true)
      return true
    }
    else {
      return false
    }
  }

  _processForm () {
    let url = this.registrationEndpoint
    let data = ko.toJS(this)
    this.submittingForm(true)

    // Simulate server processing time, just for the demo
    setTimeout(() => {
      $.post(url, data)
      .done(() => {
        this.submitError(false)
        this.completed(true)
      })
      .fail(() => {
        this.submitError(true)
        this.completed(false)
      })
      .always(() => {
        this.submittingForm(false)
      })
    }, 4000)
  }
}

export default class RegistrationView {
  mount () {
    $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
      timeout: 10000
    })

    let rvm = new RegistrationViewModel(REGISTRATION_ENDPOINT)
    ko.applyBindings(rvm)
  }
}
