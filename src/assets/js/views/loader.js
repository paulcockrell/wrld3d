import RegistrationView from './registration-form'

const views = {
  RegistrationView
}

export default function loadView (viewName) {
  return views[viewName]
}
