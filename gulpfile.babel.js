'use strict'

import gulp from 'gulp'
import sass from 'gulp-sass'
import babel from 'gulp-babel'
import haml from 'gulp-haml'
import watch from 'gulp-watch'
import watchify from 'watchify'
import source from 'vinyl-source-stream'
import browserify from 'browserify'
import babelify from 'babelify'

const dirs = {
  src: './src',
  dest: './build'
}

const sassPaths = {
  src: `${dirs.src}/assets/sass/**/*.sass`,
  dest: `${dirs.dest}`
}

const jsPaths = {
  src: `${dirs.src}/assets/js/**/*.js`,
  dest: `${dirs.dest}`
}

const hamlPaths = {
  src: `${dirs.src}/**/*.haml`,
  dest: `${dirs.dest}`
}

const imagePaths = {
  src: `${dirs.src}/assets/images/*`,
  dest: `${dirs.dest}`
}

const paths = {
  sass: sassPaths,
  js: jsPaths,
  haml: hamlPaths,
  image: imagePaths
}

gulp.task('sass', function() {
  return gulp.src(paths.sass.src)
    .pipe(watch(paths.sass.src))
    .pipe(sass())
    .pipe(gulp.dest(paths.sass.dest))
})

function compileJs(watch) {
  let bundler = browserify({
    cache: {},
    packageCache: {},
    entries: [`${dirs.src}/assets/js/app.js`],
    debug: true,
    plugin: [watchify]
  })

  if (watch) {
    console.log("Watching for updates")
    bundler.on('update', () => {console.log("1"); bundle()})
  }

  bundle()

  function bundle() {
    return bundler
       .transform(babelify.configure({
         presets: ['es2015']
       }))
      .bundle()
      .on('error', (err) => {
        console.log(err.message)
      })
      .pipe(source('bundle.js'))
      .pipe(gulp.dest(paths.js.dest))
  }
}

gulp.task('js', function() {
  return(compileJs(true))
})

gulp.task('haml', function() {
  return gulp.src(paths.haml.src)
    .pipe(watch(paths.haml.src))
    .pipe(haml())
    .pipe(gulp.dest(paths.haml.dest))
})

gulp.task('images', function() {
  return gulp.src(paths.image.src)
    .pipe(gulp.dest(paths.image.dest))
})

gulp.task('default', ['sass', 'js', 'haml', 'images'])
