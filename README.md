# WRLD3D Code test

## Dependencies

NodeJS
```
$> curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
$> sudo apt-get install -y nodejs build-essential
```

GULP
```
$> sudo npm install -g gulp
```

## Source
```
$> git clone https://paulcockrell@bitbucket.org/paulcockrell/wrld3d.git
```

## Build

The project requires transpiling from ES6, HAML, and SASS. This can be done by running the following command.
The gulp task will watch the files and recompile them when they are modified
```
$> cd /path/to/project/root
$> gulp
```

## To run live example

This is a client side only project, so can be simply run directly in the browser once the build step has
been completed.
```
$> cd /path/to/project/root
$> firefox build/registration-form.html
```
